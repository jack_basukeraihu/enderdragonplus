package org.bitbucket.jack_basukeraihu;

import java.util.ArrayList;
import java.util.Collections;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.TNTPrimed;

public class AttackSelection {

	public static void Attack(final Entity e) {
		ArrayList<String> attack = new ArrayList<String>();
		attack.add("TNT");
		attack.add("SummonWither");
		attack.add("SummonEnderMan");
		attack.add("Thunder");
		attack.add("AutoHeal");
		Collections.shuffle(attack);
		Location l = e.getLocation();
		int y = getGroundPos(l);

		switch (attack.get(0)) {
		case "TNT":
			TNTPrimed tnt = (TNTPrimed) e.getWorld().spawnEntity(l,EntityType.PRIMED_TNT);
			tnt.setFuseTicks(5 * 20);
			tnt.setYield(6);
			break;
		case "SummonWither":
			l.subtract(0, 3, 0);
			e.getWorld().spawnEntity(l, EntityType.WITHER);
			break;
		case "SummonEnderMan":
			l.setY(y);
			e.getWorld().spawnEntity(l, EntityType.ENDERMAN);
			e.getWorld().spawnEntity(l, EntityType.ENDERMAN);
			e.getWorld().spawnEntity(l, EntityType.ENDERMAN);
			e.getWorld().spawnEntity(l, EntityType.ENDERMAN);
			e.getWorld().spawnEntity(l, EntityType.ENDERMAN);
			break;
		case "Thunder":
			l.setY(y);
			TNTPrimed thunder = (TNTPrimed) e.getWorld().spawnEntity(l,EntityType.PRIMED_TNT);
			thunder.setFuseTicks(0);
			e.getWorld().strikeLightning(l);
			break;
		case "AutoHeal":
			int hp = (int) (Math.random() * 20);
			LivingEntity dragon = (LivingEntity) e;
			if (dragon.getHealth() + hp >= 200) {
				return;
			}
			dragon.setHealth(dragon.getHealth() + hp);
			break;
		}

	}

	private static int getGroundPos(Location loc) {
		loc = loc.getWorld().getHighestBlockAt(loc).getLocation();
		int ground = loc.getBlockY();
		for (int y = loc.getBlockY(); y != 0; y--) {
			loc.setY(y);
			if (loc.getBlock().getLightFromSky() >= 8
					&& !loc.getBlock().getType().isSolid()
					&& !loc.clone().add(0, 1, 0).getBlock().getType().isSolid()) {
				ground = y;
			}
		}
		return ground;
	}

}
