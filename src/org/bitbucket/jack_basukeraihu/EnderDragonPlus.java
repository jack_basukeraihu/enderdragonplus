package org.bitbucket.jack_basukeraihu;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class EnderDragonPlus extends JavaPlugin {

	public Logger logger = Logger.getLogger("Mineraft");

	public void onEnable() {
		if (!PluginCheck()) {
			return;
		}
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new BukkitListener(this), this);
		PluginDescriptionFile yml = getDescription();
		this.logger.info("[" + yml.getName() + "] v" + yml.getVersion()+ " が有効になりました");
		saveDefaultConfig();
	}

	public void onDisable() {
		PluginDescriptionFile yml = getDescription();
		this.logger.info("[" + yml.getName() + "] v" + yml.getVersion()+ " が無効になりました");
	}

	private boolean PluginCheck() {
		if (!Bukkit.getPluginManager().isPluginEnabled("BKCommonLib")) {
			getLogger().warning("BKCommonLibが読み込まれていません。プラグインを停止します。");
			setEnabled(false);
			return false;
		}
		return true;
	}
}
