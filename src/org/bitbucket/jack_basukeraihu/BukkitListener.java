package org.bitbucket.jack_basukeraihu;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;

import com.bergerkiller.bukkit.common.events.EntityMoveEvent;

public class BukkitListener implements Listener {

	public static EnderDragonPlus plugin;

	public BukkitListener(EnderDragonPlus instance) {
		plugin = instance;
	}

	Boolean Action = true;

	@EventHandler
	public void EntityMove(EntityMoveEvent e) {
		if (e.getEntityType() != EntityType.ENDER_DRAGON) {
			return;
		}
		if (!Action) {
			return;
		}
		Entity en = e.getEntity();
		AttackSelection.Attack(en);
		int time = plugin.getConfig().getInt("ActionInterval");
		Interval(time);
		Action = false;
	}

	@EventHandler
	public void Target(EntityTargetLivingEntityEvent e) {
		if (e.getEntityType() == EntityType.WITHER) {
			if (!(e.getTarget() instanceof Player)) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void EntityDamage(EntityDamageEvent e) {
		if (e.getEntityType() != EntityType.ENDER_DRAGON) {
			return;
		}
		if (e.getCause() == DamageCause.THORNS) {
			e.setCancelled(true);
		}
	}

	public void Interval(final int step) {
		if (step > 0) {
			Bukkit.getServer().getScheduler().runTaskLater(plugin, new Runnable() {
			public void run() {
				Interval(step - 1);
			}
			}, 20L);
		} else {
			Action = true;
		}
	}

}
